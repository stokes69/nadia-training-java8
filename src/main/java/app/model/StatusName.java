package app.model;

public enum StatusName {
    IN_CIRCULATION, OUT_CIRCULATION, WRITTEN_OFF
}
