package app.model;

import lombok.*;

import java.time.Instant;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor @ToString
public class Status {
    private StatusName name;
    private Instant startDate;
    private Instant endDate;
    private Long id;
}
