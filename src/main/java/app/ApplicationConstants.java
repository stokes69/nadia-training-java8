package app;

import java.util.UUID;

public class ApplicationConstants {

    public final static int COMMAND_LIST_SIZE = 11;

    public final static UUID UUID_QUERY = UUID.fromString("f6057299-323a-4f3a-887a-4e36dd770807");

    public final static double AMOUNT_QUERY = 117.68;
}
