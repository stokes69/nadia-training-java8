package app.problem;

import java.util.HashSet;
import java.util.Set;

public class One {

    public static Set<Pair> compute(int[] tableA, int[] tableB, int target) {
        Set<Pair> paires = new HashSet<>();
        for (int i : tableA) {
            for (int j : tableB) {
                if (i + j == target){
                    Pair p = makePair(i, j);
                    paires.add(p);
                }
            }
        }
        return paires;
    }

    public static void main(String[] args) {
        Set<Pair> set = new HashSet<>();
        set.add(makePair(1, 2));
        set.add(makePair(2, 1));
        set.forEach(System.out::println);
    }


    public static Pair makePair (int A, int B){
        return new Pair(A, B);
    }


}
