package app.problem;

import java.util.HashMap;
import java.util.Map;

public class MapTry {

    public static Map<String, Integer> generate() {
        Map<String, Integer> results = new HashMap<>();
        results.put("Sofia", 18);
        results.put("toto", 0);
        results.put("tii", 10);
        return results;
    }
}
