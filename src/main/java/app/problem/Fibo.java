package app.problem;

public class Fibo {

    public static int fib(int a) {
        if (a <= 1) {
           return 1;
        }
        int[] memo = {1, 1};
        for (int i = 2; i < a; ++i) {
            int fib = memo[0] + memo[1];
            memo [0] = memo[1];
            memo[1] = fib;
        }
        return memo [1];
    }

    public static int fib(int a, int[] memo) {
        int result;
        if (memo[a] != 0) {
            return memo[a];
        }
        if (a <= 1) {
            result =  1;
        } else {
            result = fib(a - 1, memo) + fib(a - 2, memo);
        }
        memo[a] = result;
        return result;

    }

}
