package app.problem;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class StringProblems {

    // EASY
    static short numberOfWordsInString(String s) {
        s = " " + s;
        short count = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s.charAt(i) != ' ') {
                if (i > 0 && s.charAt(i - 1) == ' ') {
                    ++count;
                }
            }
        }
        return count;
    }

    static List<Character> findDuplicateCharacterInString(String s) {
        List<Character> characters = new ArrayList<>();
        for (int i = 0; i < s.length(); ++i) {
            char current = s.charAt(i);
            if (characters.contains(current)) {
                continue;
            }
            for (int j = i + 1; j < s.length(); ++j) {
                char next = s.charAt(j);
                if (current == next) {
                    characters.add(current);
                    break;
                }
            }
        }
        return characters;
    }

    // MEDIUM
    static boolean areStringsAnagram(String first, String second) {
        return false;
    }

    static String reverseEachWordInString(String s) {
        List<String> list = new ArrayList<>();
        String[] parts = s.split(" ");
        for (int i = 0; i < parts.length; ++i) {
            String answer = "";
            String part = parts[i];
            for (int j = part.length() - 1; j >= 0; --j) {
                answer = answer + part.charAt(j);
            }
            parts[i] = answer;
        }
        for (String ss : parts) {
            list.add(ss);
        }
        return String.join(" ", list);
    }

    // HARD
    static boolean doesParenthesesMatch(String s) {
        Stack stack = new Stack();
        for (int i = 0; i < s.length(); ++i) {
            char current = s.charAt(i);
            if (current == '(') {
                stack.push(true);
            } else if(current == ')'){
                if (stack.empty()){
                    return false;
                }
                stack.pop();
            }
        }
        return stack.empty();
    }

}
