package app.problem;


import lombok.extern.log4j.Log4j2;

import java.util.*;

@Log4j2
public class Pendu {

    public static List<String> words;

    public Scanner scanner = new Scanner(System.in);

    static {
        words = Arrays.asList(
                "guigui",
                "nadia",
                "sofia"
        );
    }
    boolean [] lettreTrouvee;
    private String motMystere;
    boolean jeuEnCours;


    public String piocher() {
        int number = Math.abs(new Random().nextInt() % words.size());
        return words.get(number);
    }

    public void run() {
        jeuEnCours = true;
        motMystere = piocher();
        lettreTrouvee = new boolean[motMystere.length()];
        afficherMotMystere();
        deviner();


    }

    private void deviner() {
        while (jeuEnCours) {

            String input = scanner.nextLine();
            char c = input.charAt(0);
            for (int i = 0; i < motMystere.length(); ++i) {
                if (motMystere.charAt(i) == c) {
                    lettreTrouvee[i] = true;
                }
            }
            verifierFinDePartie();
            afficherMotMystere();
        }
    }

    void verifierFinDePartie() {
        jeuEnCours = false;
        for (int i = 0; i < lettreTrouvee.length; ++i) {
            if (!lettreTrouvee[i]) {
                jeuEnCours = true;
                break;
            }
        }
    }

    private void afficherMotMystere() {
        List<String> affichage = new ArrayList<>();
        for (int i = 0; i < motMystere.length(); ++i) {
            if (lettreTrouvee[i]) {
                affichage.add(String.valueOf(motMystere.charAt(i)));
            } else {
                affichage.add("_");
            }
        }
        System.out.println(String.join(" ", affichage));
    }


    public static void main(String[] args) {
       Pendu pendu = new Pendu();
       pendu.run();
    }


}
