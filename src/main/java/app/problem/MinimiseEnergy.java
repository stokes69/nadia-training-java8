package app.problem;

import java.util.Arrays;
import java.util.List;

public class MinimiseEnergy {

    public static int minimize(List<Integer> energy) {
        int[] m = new int[energy.size() + 1];
        m[0] = 0;
        m[1] = energy.get(0);
        for (int i = 2; i < m.length; i++) {
            m[i] = energy.get(i - 1) + Math.min(m[i - 2], m[i - 1]);
        }
        return m[m.length - 1];
    }

    /**
     * On peut aller de 1, 2 ou 3 step en avant
     *
     * @param goal
     * @return
     */
    public static int waysOfJumpToStep(final int goal) {
        int[] marches = new int[3];
        for (int i = 0; i < goal; ++i) {
            int maxSoFar = 1 + Arrays.stream(marches).sum();
            marches[0] = marches[1];
            marches[1] = marches[2];
            marches[2] = maxSoFar;
        }
        return marches[2];
    }
}
