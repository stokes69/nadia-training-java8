package app.problem;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Pair {

    Integer first;
    Integer second;

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Pair)) {
            return false;
        }

        if (this.first.equals(((Pair) other).second) && (this.second.equals(((Pair) other).first))){
            return true;
        }
        if (this.first.equals(((Pair) other).first) && (this.second.equals(((Pair) other).second))){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
