package app.problem;

import app.model.Status;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Fusion {

    /**
     *
     * @param oldStatusList L'ancienne liste de statuts
     * @param newStatus un status à ajouter dans la liste
     * @return la nouvelle liste
     */
    static List<Status> aggregate(List<Status> oldStatusList, Status newStatus) {

        // create a new list
        List<Status> newStatusesList = new ArrayList<>();

        // sort by starting date
        oldStatusList.sort(Comparator.comparing(Status::getStartDate));

        oldStatusList.forEach(oldStatus -> {
            if (includeInterval(newStatus, oldStatus)) {
                newStatusesList.add(newStatus);
            } else if (intersectInterval(newStatus, oldStatus)) {
                if (newStatus.getName().equals(oldStatus.getName())) {
                    // fusionner
                    // get the min start date between new and old
                    Instant startDate = oldStatus.getStartDate().isBefore(newStatus.getStartDate()) ?
                            oldStatus.getStartDate()
                            : newStatus.getStartDate();
                    // get the max end date between new and old
                    Instant endDate = oldStatus.getEndDate().isAfter(newStatus.getEndDate()) ?
                            oldStatus.getEndDate()
                            : newStatus.getEndDate();

                    // affect to oldStatus
                    oldStatus.setStartDate(startDate);
                    oldStatus.setEndDate(endDate);

                } else {
                    // changer les dates du old et l'ajouter aussi
                    if (oldStatus.getStartDate().isBefore(newStatus.getStartDate())) {
                        oldStatus.setEndDate(newStatus.getStartDate());
                    } else {
                        oldStatus.setStartDate(newStatus.getEndDate());
                    }
                }
                // add old to the list
                newStatusesList.add(oldStatus);
            }else if(includeInterval(oldStatus, newStatus)){
                if (!oldStatus.getName().equals(newStatus.getName())){
                    Instant endDate = oldStatus.getEndDate();
                    oldStatus.setEndDate(newStatus.getStartDate());
                    Status newCreated = Status.builder().name(oldStatus.getName()).startDate(newStatus.getEndDate()).endDate( endDate).build();
                   newStatusesList.add(newStatus);
                   newStatusesList.add(newCreated);
                }
                newStatusesList.add(oldStatus);
            }

            else {
                newStatusesList.add(oldStatus);

            }
        });
        return newStatusesList;
    }

    static boolean includeInterval(Status big, Status little) {
        if (!big.getStartDate().isAfter(little.getStartDate())) {
            if(!big.getEndDate().isBefore(little.getEndDate())){
                return true;
            }
        }
        return false;
    }

    static boolean intersectInterval(Status s1, Status s2) {
        if(isBetween(s1.getStartDate(), s2.getStartDate(), s2.getEndDate())){
            return s2.getEndDate().isBefore(s1.getEndDate());
        }
        if(isBetween(s1.getEndDate(), s2.getStartDate(), s2.getEndDate())){
            return s2.getStartDate().isAfter(s1.getStartDate());
        }
        return false;
    }

    static boolean isBetween (Instant date, Instant min, Instant max ){
        return !date.isBefore(min) && !date.isAfter(max);
    }

}
