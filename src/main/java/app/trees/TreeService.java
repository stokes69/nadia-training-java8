package app.trees;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class TreeService {

    static void BFSearch(Node root) {

        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node node = queue.poll();
            System.out.println(node.data);
            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }
        }
    }

    static<T> List<T> DepthFirstSearch(Node<T> root) {
        ArrayList<T> path = new ArrayList<>();
        DepthFirstSearchRec(root, path);
        return path;
    }

    private static<T> void DepthFirstSearchRec(Node<T> root, List<T> path) {
        if (root == null) {
            return;
        }
        DepthFirstSearchRec(root.left, path);
        path.add(root.data);
        DepthFirstSearchRec(root.right, path);
    }

    static <T> int depth(Node<T> root) {
        return depthR(root, 0);
    }

    private static <T> int depthR(Node<T> root, int currentDepth) {
        if (root == null) {
            return currentDepth - 1;
        }
        return Math.max(
                depthR(root.left, currentDepth + 1),
                depthR(root.right, currentDepth + 1));
    }
}
