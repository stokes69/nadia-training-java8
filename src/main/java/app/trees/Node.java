package app.trees;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Node<T>
{   public T data;
    public Node<T> left;
    public Node<T> right;
}
