package app.trees;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Tree<T> {
    public Node<T> root;
}
