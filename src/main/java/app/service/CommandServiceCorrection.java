package app.service;

import app.model.Command;
import app.repository.CommandRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class CommandServiceCorrection implements ICommandService {

    private CommandRepository commandRepository;

    public CommandServiceCorrection() {
        this.commandRepository = new CommandRepository();
    }
    @Override
    public List<Command> findAll() {
        return this.commandRepository.findAll();
    }

    @Override
    public List<Command> findAllCommandsWithAmountGreaterOrEqualsThan(double minimalAmount) {
        return findAll().stream()
                .filter(command -> command.getAmount() >= minimalAmount)
                .collect(Collectors.toList());
    }

    @Override
    public List<Command> findAllCommandsWithAmountLessOrEqualsThan(double maximalAmount) {
        return findAll().stream()
                .filter(command -> command.getAmount() <= maximalAmount)
                .collect(Collectors.toList());
    }

    @Override
    public List<Command> findAllSortedByUUID() {
        return findAll().stream()
                .sorted(Comparator.comparing(Command::getUuid))
                .collect(Collectors.toList());
    }

    @Override
    public List<Double> findAmountsSorted() {
        return findAll().stream()
                .map(Command::getAmount)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Command> findMostExpensive() {
        return findAll().stream()
                .max(Comparator.comparing(Command::getAmount));
    }

    @Override
    public Optional<Command> findLeastExpensive() {
        return findAll().stream()
                .min(Comparator.comparing(Command::getAmount));
    }

    @Override
    public List<UUID> findAllUUID() {
       return findAll().stream()
               .map(Command::getUuid)
               .collect(Collectors.toList());
    }

    @Override
    public long findNumberOfCommands() {
        return findAll().size();
    }


    @Override
    public long findNumberOfCommandsWithAmountBetween(double min, double max) {
        return findAll().stream()
                .filter(command -> min <= command.getAmount() && command.getAmount() <= max)
                .count();
    }

    @Override
    public double sumAllAmounts() {
        return findAll().stream()
                .map(Command::getAmount)
                .reduce(0.0, (currentTotal, nextAmount) -> currentTotal + nextAmount)
                // ou bien  .reduce(0.0, Double::sum)
                ;
    }

    @Override
    public Optional<Command> findByUUID(UUID uuid) {
        return findAll().stream()
                .filter(command -> command.getUuid().equals(uuid))
                .findFirst();
    }
}
