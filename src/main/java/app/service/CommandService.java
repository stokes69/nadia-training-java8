package app.service;

import app.model.Command;
import app.repository.CommandRepository;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Log4j2
public class CommandService implements ICommandService{

    private CommandRepository commandRepository;

    public CommandService() {
        log.trace("new Service");
        this.commandRepository = new CommandRepository();
    }

    @Override
    public List<Command> findAll() {
        return this.commandRepository.findAll();
    }

    @Override
    public List<Command> findAllCommandsWithAmountGreaterOrEqualsThan(double minimalAmount) {

        return findAll().stream()
                .filter(new Predicate<Command>() {
                    @Override
                    public boolean test(Command command) {
                        return command.getAmount() >= minimalAmount;
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<Command> findAllCommandsWithAmountLessOrEqualsThan(double maximalAmount) {
      return findAll().stream().filter(command -> command.getAmount()<= maximalAmount)
              .collect(Collectors.toList());
    }

    @Override
    public List<Command> findAllSortedByUUID() {
          /*return findAll().stream()
                  .sorted(Comparator.comparing(Command::getUuid))
                  .collect(Collectors.toList());*/

        List<Command> list = findAll();
        for (int i = 0; i < list.size(); i++) {
            Command min = list.get(i);
            int minIndex = i;
            for (int j = i+1; j < list.size(); j++) {
                if (list.get(j).getUuid().compareTo(min.getUuid()) <0){
                    min = list.get(j);
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                Collections.swap(list, i, minIndex);
            }

        }
        return list;
    }

    @Override
    public List<Double> findAmountsSorted() {
       return findAll().stream().map(Command::getAmount)
                .sorted()
               .collect(Collectors.toList());
    }

    @Override
    public Optional<Command> findMostExpensive() {
        List<Command> commands = findAll();
   /*     if (commands.isEmpty()) {
            return Optional.empty();
        }
        Command max = commands.get(0);
        for (int i=1; i<commands.size();i++){
            Command new_command = commands.get(i);
            if (max.getAmount() < new_command.getAmount()) {
                max = new_command;
            }
        }*/
      //  return Optional.of(max);

        return commands.stream()
                .max(Comparator.comparing(Command::getAmount));
    }

    @Override
    public Optional<Command> findLeastExpensive() {
       return findAll().stream()
               .min(Comparator.comparing(Command::getAmount));
    }

    @Override
    public List<UUID> findAllUUID() {
        return findAll().stream().map(Command::getUuid).collect(Collectors.toList());
    }

    @Override
    public long findNumberOfCommands() {
     return findAll().size();
    }

    @Override
    public long findNumberOfCommandsWithAmountBetween(double min, double max) {
         return findAll().stream()
                 .map(Command::getAmount)
                 .filter(amount -> min <= amount && amount <= max)
                 .count();
    }

    @Override
    public double sumAllAmounts() {
        return findAll().stream()
                .map(Command::getAmount)
                .reduce(0.0, Double::sum);
    }

    @Override
    public Optional<Command> findByUUID(UUID uuid) {
       return findAll().stream()
               .filter(command -> command.getUuid().equals(uuid))
               .findFirst();
    }

    public String getUUIDUppercase(Command command) {
        return Optional.ofNullable(command)
                .filter(cmd -> cmd.getAmount() > 0)
                .map(Command::getUuid)
                .map(uuid -> uuid.toString().toUpperCase())
                .orElse(null);
    }
}
