package app.repository;

import app.ApplicationConstants;
import app.model.Command;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
public class CommandRepository {
    public CommandRepository() {
       log.trace("new Repository");
    }

    public List<Command> findAll() {
        List<Command> commands = new ArrayList<>();

        commands.add(new Command(ApplicationConstants.UUID_QUERY, ApplicationConstants.AMOUNT_QUERY));

        for (int i = 1; i < ApplicationConstants.COMMAND_LIST_SIZE; ++i) {
            commands.add(new Command(UUID.randomUUID(), i * 100));
        }

        return commands;
    }

}
