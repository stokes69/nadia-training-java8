package app.problem;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class MapTryTest {

    /**
     * Afficher :
     * Nom => note / 20 \n
     */
    @Test
    public void test_map() {
        Map<String, Integer> mapTest = MapTry.generate();
        for (Map.Entry<String, Integer> entry : mapTest.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
        }
        mapTest.forEach((key, value) -> {
            String msg = key +
                    " => " +
                    value +
                    " / 20";
            System.out.println(msg);
        });
        assertThat(new ArrayList<>()).isEmpty();
    }

    @Test
    public void note_Sofia() {
        Map<String, Integer> map = MapTry.generate();
        System.out.println(map.get( "Sofia"));
    }
    @Test
    public void note_xxx() {
        Map<String, Integer> map = MapTry.generate();
        Integer newNote = map.computeIfAbsent("Sofia", s -> map.get("Sofia"));
        System.out.println(newNote);

        String input = "xxy";
        Integer answer;
        answer = map.getOrDefault(input, -1);
        assertThat(answer).isEqualTo(-1);
    }
}
