package app.problem;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PenduTest {

    private Pendu pendu;

    @Before
    public void init() {
        pendu = new Pendu();
    }

    @Test
    public void piocher() {
        String mot;
        for (int i = 0; i < 1e9; ++i) {
             mot = pendu.piocher();
            assertThat(mot).isNotBlank();
        }
    }

    @Test
    public void jeuEnCours() {
        pendu.lettreTrouvee = new boolean[]{true, true, false};
        pendu.verifierFinDePartie();
        assertThat(pendu.jeuEnCours).isTrue();
    }
    @Test
    public void JeuTermine() {
        pendu.lettreTrouvee = new boolean[]{true, true, true};
        pendu.verifierFinDePartie();
        assertThat(pendu.jeuEnCours).isFalse();
    }
}
