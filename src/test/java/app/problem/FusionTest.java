package app.problem;


import app.model.Status;
import app.model.StatusName;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class FusionTest {
    List<Status> statuses;

    @Test
    public void dates() {

        Instant nowInstant = Instant.now();
        OffsetDateTime nowOffset = OffsetDateTime.now();
        ZonedDateTime zonedDateTime = nowOffset.atZoneSameInstant(ZoneId.of("Australia/Sydney"));
        // 1977-04-22T01:00:00-05:00
     //  log.info(nowInstant);
      // log.info(nowOffset);
       log.info("Heure en Australie : {}", zonedDateTime);
    }


    @Before
    public void initList() {
        statuses = new ArrayList<>();
        Instant firstEndDate = Instant.parse("2020-01-01T00:00:00Z");
        Status status1 = Status.builder()
                .id(1L)
                .startDate(Instant.now())
                .endDate(firstEndDate)
                .name(StatusName.IN_CIRCULATION)
                .build();
/*        Status status2 = Status.builder()
                .id(1L)
                .startDate(firstEndDate)
                .endDate(null)
                .name(StatusName.WRITTEN_OFF)
                .build();
        statuses.add(status2);

 */
        statuses.add(status1);
    }

    /*-
    *        |___________|01/01/2020  IN 1
    *        |_________________|   OFF
    *
    *        |_________________| OFF
     */
    @Test
    public void agg1() {
        Status status = Status.builder()
                .id(11L)
                .startDate(Instant.now().minus(7, ChronoUnit.DAYS))
                .endDate(Instant.parse("2021-01-01T00:00:00Z"))
                .name(StatusName.WRITTEN_OFF)
                .build();

        List<Status> newStatuses = Fusion.aggregate(this.statuses, status);
        log.debug(newStatuses);
    }
    /*-
    *        |___________|01/01/2020  IN 1
    *        |_________________|   OFF
    *
    *        |_________________| OFF
     */
    @Test
    public void agg2() {

        Status status = Status.builder()
                .id(11L)
                .startDate(Instant.now().minus(7, ChronoUnit.DAYS))
                .endDate(Instant.parse("2021-01-01T00:00:00Z"))
                .name(StatusName.WRITTEN_OFF)
                .build();
        Status status2 = Status.builder()
                .id(2L)
                .name(StatusName.WRITTEN_OFF)
                .startDate(Instant.now().minus(15, ChronoUnit.DAYS))
                .endDate(Instant.now().plus(15, ChronoUnit.DAYS))
                .build();
        statuses.add(status2);
        List<Status> newStatuses = Fusion.aggregate(this.statuses, status);
        log.debug(newStatuses);
    }

}
