package app.problem;

import org.junit.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


public class OneTest {

    @Test
    public void compute() {
        int[] tableA = {1,2,2,2};
        int[] tableB = {1,2,2,2};
        int target = 4;
        Set<Pair> answer = One.compute(tableA, tableB, target);
        assertThat(answer).hasSize(1);
        assertThat(answer.iterator().next().first).isEqualTo(2);
        assertThat(answer.iterator().next().second).isEqualTo(2);
    }
}
