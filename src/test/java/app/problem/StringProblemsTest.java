package app.problem;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class StringProblemsTest {

    @Test
    public void numberOfWordsInString() {
        short answer = StringProblems.numberOfWordsInString("je suis un petit coquelicot");
        assertThat(answer).isEqualTo((short)5);
    }

    @Test
    public void findDuplicateCharacterInString() {
        List<Character> answer = StringProblems.findDuplicateCharacterInString("guillaume");
        assertThat(answer).hasSize(2);
        assertThat(answer).contains('u', 'l');
    }


    @Test
    public void reverseEachWordInString() {
        String answer = StringProblems.reverseEachWordInString("je suis fatigué par tes bêtises");
        assertThat(answer).isEqualTo("ej sius éugitaf rap set sesitêb");
    }

    @Test
    public void doesParenthesesMatch1() {
        boolean answer = StringProblems.doesParenthesesMatch("((1+2)*8)/(78-7)");
        assertThat(answer).isTrue();
    }
    @Test
    public void doesParenthesesMatch2() {
        boolean answer = StringProblems.doesParenthesesMatch("((1+2)*8/(78-7)");
        assertThat(answer).isFalse();
    }
    @Test
    public void doesParenthesesMatch3() {
        boolean answer = StringProblems.doesParenthesesMatch("((1+2)*8)/78-7)");
        assertThat(answer).isFalse();
    }

}
