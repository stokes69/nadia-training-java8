package app.problem;


import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
public class MinimiseEnergyTest {

    @Test
    public void minimize() {
        List<Integer> energy = Arrays.asList(1, 1, 1, 1, 1, 1, 1);
        int answer = MinimiseEnergy.minimize(energy);
        assertThat(answer).isEqualTo(4);
    }

    @Test
    public void step1() {
        int goal = 1;
        int answer = MinimiseEnergy.waysOfJumpToStep(goal);
        assertThat(answer).isEqualTo(1);
    }

    @Test
    public void step2() {
        int goal = 2;
        int answer = MinimiseEnergy.waysOfJumpToStep(goal);
        assertThat(answer).isEqualTo(2);
    }

    @Test
    public void step3() {
        int goal = 3;
        int answer = MinimiseEnergy.waysOfJumpToStep(goal);
        assertThat(answer).isEqualTo(4);
    }
    @Test
    public void step4() {
        int goal = 20;
        int answer = MinimiseEnergy.waysOfJumpToStep(goal);
        System.out.println(answer);
        // assertThat(answer).isEqualTo(4);
    }
}
