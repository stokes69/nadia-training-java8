package app.trees;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

import static org.assertj.core.api.Assertions.assertThat;

public class TreeServiceTest {

    Tree<Character> tree;

    @Before
    public void init() {
        Node<Character> nodeA = new Node<>('A', null, null);
        Node<Character> nodeB = new Node<>('B', null, null);
        Node<Character> nodeC = new Node<>('C', null, null);
        Node<Character> nodeD = new Node<>('D', null, null);
        Node<Character> nodeE = new Node<>('E', null, null);
        Node<Character> nodeF = new Node<>('F', null, null);
        Node<Character> nodeG = new Node<>('G', null, null);
        tree = new Tree<>(nodeA);
        nodeA.left = nodeB;
        nodeA.right = nodeC;
        nodeC.left = nodeE;
        nodeC.right = nodeD;
        nodeB.left = nodeF;
        nodeF.right = nodeG;
    }

    @Test
    public void test() {
        TreeService.BFSearch(tree.root);
    }

    @Test
    public void dfs() {
        List<Character> path = TreeService.DepthFirstSearch(tree.root);
        path.forEach(System.out::println);
    }

    @Test
    public void testQueue() {
        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        while(!queue.isEmpty()){
            System.out.println(queue.poll());
        }
    }

    @Test
    public void depth() {
        int depth = TreeService.depth(tree.root);
        assertThat(depth).isEqualTo(3);
    }
}
