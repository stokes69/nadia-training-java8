package app;

import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class OptionalTest {

    @Test
    public void empty() {
        Optional<String> optional = Optional.empty();

        assertThat(optional).isNotPresent();

    }
    @Test
    public void nonEmpty() {
        Optional<String> optional = Optional.of("text");

        assertThat(optional).isPresent();

    }
    @Test
    public void nullable() {

        Optional<String> optional = Optional.ofNullable(null);
        String aDefault = optional.orElse("default");
        assertThat(optional).isNotPresent();

        optional = Optional.ofNullable("text");
        assertThat(optional).isPresent();

    }
}
