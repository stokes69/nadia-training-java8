package app.service;

import app.ApplicationConstants;
import app.model.Command;
import app.utils.Correction;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Log4j2
@Correction(value = false)
public class CommandServiceTest {
    private ICommandService commandService;

    @Before
    public void setUp() throws Exception {
        boolean isCorrection = getClass().getAnnotation(Correction.class).value();
        if (isCorrection) {
            this.commandService = new CommandServiceCorrection();
        } else {
            this.commandService = new CommandService();
        }
    }

    @Test
    public void findAll() {
        List<Command> all = this.commandService.findAll();
        assertThat(all).hasSize(ApplicationConstants.COMMAND_LIST_SIZE);
    }

    @Test
    public void findAllCommandsWithAmountGreaterOrEqualsThan() {
        double minAmount = 150.0;
        List<Command> commands = this.commandService.findAllCommandsWithAmountGreaterOrEqualsThan(minAmount);
        for (Command command : commands) {
            assertThat(command.getAmount()).isGreaterThanOrEqualTo(minAmount);
        }
    }

    @Test
    public void findAllCommandsWithAmountLessOrEqualsThan() {
        double maxAmount = 150.0;
        List<Command> commands = this.commandService.findAllCommandsWithAmountLessOrEqualsThan(maxAmount);
        for (Command command : commands) {
            assertThat(command.getAmount()).isLessThanOrEqualTo(maxAmount);
        }
    }

    @Test
    public void findMostExpensive() {
        Optional<Command> mostExpensive = this.commandService.findMostExpensive();
        assertThat(mostExpensive).isPresent();
        Command command = mostExpensive.get();
        for (Command c : this.commandService.findAll()) {
            assertThat(command.getAmount()).isGreaterThanOrEqualTo(c.getAmount());
        }
    }

    @Test
    public void findLeastExpensive() {
        Optional<Command> lessExpensive = this.commandService.findLeastExpensive();
        assertThat(lessExpensive).isPresent();
        Command command = lessExpensive.get();
        for (Command c : this.commandService.findAll()) {
            assertThat(command.getAmount()).isLessThanOrEqualTo(c.getAmount());
        }
    }

    @Test
    public void findAllUUID() {
        List<UUID> allUUID = this.commandService.findAllUUID();
        assertThat(allUUID).hasSize(ApplicationConstants.COMMAND_LIST_SIZE);

    }

    @Test
    public void findAllSortedByUUID() {
        List<Command> allSortedByUUID = this.commandService.findAllSortedByUUID();
        assertThat(allSortedByUUID).isSortedAccordingTo(Comparator.comparing(Command::getUuid));
    }

    @Test
    public void findAmountsSorted() {
        List<Double> amountsSorted = this.commandService.findAmountsSorted();
        assertThat(amountsSorted).hasSize(ApplicationConstants.COMMAND_LIST_SIZE);
        assertThat(amountsSorted).isSorted();
    }

    @Test
    public void findNumberOfCommands() {
        long numberOfCommands = this.commandService.findNumberOfCommands();
        assertThat(numberOfCommands).isEqualTo(ApplicationConstants.COMMAND_LIST_SIZE);
    }

    @Test
    public void sumAllAmounts() {
        double total = this.commandService.sumAllAmounts();
        double expected = 0.0;
        List<Command> all = this.commandService.findAll();
        for (Command command : all) {
            expected += command.getAmount();
        }

        log.debug(total);

        assertThat(total).isEqualTo(expected);
    }

    @Test
    public void findByUUID() {
        UUID uuid =ApplicationConstants.UUID_QUERY;
        Optional<Command> byUUID = this.commandService.findByUUID(uuid);
        Command command = byUUID.get();

        assertThat(command.getUuid()).isEqualTo(ApplicationConstants.UUID_QUERY);
        assertThat(command.getAmount()).isEqualTo(ApplicationConstants.AMOUNT_QUERY);

    }

    @Test
    public void findNumberOfCommandsWithAmountBetween() {
        long count = 0;
        double min = 100.0;
        double max = 700.0;
        for (Command c : this.commandService.findAll()) {
            if (min <= c.getAmount() && c.getAmount() <= max) {
                ++count;
            }
        }
        assertThat(this.commandService.findNumberOfCommandsWithAmountBetween(min, max)).isEqualTo(count);
    }

    @Test
    public void swap() {

        int a = 1, b = 0;
        int c = a;
        a =  b;
        b = c;

        assertThat(a).isEqualTo(0);
        assertThat(b).isEqualTo(1);
    }

    @Test
    public void getUUIDUppercase() {

        Command command = new Command();
        command.setUuid(UUID.randomUUID());
        command.setAmount(1);
        String uuidUppercase = ((CommandService) commandService).getUUIDUppercase(command);

        assertThat(uuidUppercase).isNotNull();

    }
}
